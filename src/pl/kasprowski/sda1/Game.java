package pl.kasprowski.sda1;

public class Game {

    private int choosePlayer() {
        int firstPlayer = (int) Math.round(Math.random());
        if (firstPlayer == 0) {
            System.out.println("Grę rozpoczyna O");
        } else {
            System.out.println("Grę rozpoczyna X");
        }
        return firstPlayer;
    }

    public void startGame() {
        int firstPlayer = this.choosePlayer();
        int secondPlayer;
        if (firstPlayer == 0) {
            secondPlayer = 1;
        } else {
            secondPlayer = 0;
        }
        Board board = new Board();
        do {
            if (!board.checkResults()) {
                board.setField(firstPlayer);
                board.displayBoard();
            } else {
                return;
            }
            if (!board.checkResults()) {
                board.setField(secondPlayer);
                board.displayBoard();
            } else {
                return;
            }
        } while (true);
    }
}
