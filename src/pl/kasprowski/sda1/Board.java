package pl.kasprowski.sda1;

public class Board {
    Input input = new Input();
    private int[][] board = new int[3][3];
    int counter = 0;

    public Board() {
        this.setInitialBoard();
        this.displayBoard();
    }

    private void setInitialBoard() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                board[j][i] = 0;
            }
        }
    }

    public boolean checkResults() {
        if (board[1][1] == board[0][0] && board[1][1] == board[2][2] && board[1][1] != 0 || board[1][1] == board[0][2] && board[1][1] == board[2][0] && board[1][1] != 0) {
            System.out.println("Koniec gry!");
            return true;
        }
        for (int i = 0; i < 3; i++) {
            if (board[i][0] == board[i][1] && board[i][0] == board[i][2] && board[i][0] != 0) {
                System.out.println("Koniec gry!");
                return true;
            }
            if (board[0][i] == board[1][i] && board[0][i] == board[2][i] && board[0][i] != 0) {
                System.out.println("Koniec gry!");
                return true;
            }
        }
        if (counter == 9) {
            System.out.println("Remis!");
            return true;
        }
        return false;
    }

    public void setField(int x) {
        do {
            int[] playerInput = input.getData();
            int row = playerInput[1] - 1;
            int col = playerInput[0] - 1;

            if (board[col][row] == 0) {
                switch (x) {
                    case 0:
                        board[col][row] = 1;
                        counter++;
                        break;
                    case 1:
                        board[col][row] = 2;
                        counter++;
                        break;
                }
                return;
            } else {
                System.out.println("To pole jest zajęte!");
            }
        } while (true);
    }

    public void displayBoard() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (board[j][i] == 0) {
                    System.out.print(" ");
                } else if (board[j][i] == 1) {
                    System.out.print("O");
                } else if (board[j][i] == 2) {
                    System.out.print("X");
                }
                if (j == 2) {
                    continue;
                }
                System.out.print("|");
            }
            System.out.println();
            if (i != 2) {
                System.out.println("-----");
            }
        }
    }
}
