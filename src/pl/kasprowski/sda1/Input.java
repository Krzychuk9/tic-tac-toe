package pl.kasprowski.sda1;

import java.util.Scanner;
import java.util.regex.Pattern;

public class Input {
    private Scanner sc = new Scanner(System.in);

    public int[] getData() {
        int[] playerMove = new int[2];
        String regex = "[1-3]\\s[1-3]";
        boolean correct;
        do {
            System.out.println("Podaj współrzędne pola - 'kolumna wiersz (1-3)'");
            String input = sc.nextLine();
            if (Pattern.matches(regex, input)) {
                playerMove[0] = Integer.parseInt(input.substring(0, 1));
                playerMove[1] = Integer.parseInt(input.substring(2, 3));
                correct = true;
            } else {
                System.out.println("Niepoprawne współrzędne!");
                correct = false;
            }
        } while (!correct);
        return playerMove;
    }

    /*
    public int[] getData() {
        int[] playerMove = new int[2];
        boolean correct = false;
        do {
            for (int i = 0; i < 2; i++) {
                if (sc.hasNextInt()) {
                    playerMove[i] = sc.nextInt();
                    correct = false;
                } else {
                    System.out.println("Wrong numbers!(int int)");
                    sc.nextLine();
                    correct = true;
                    break;
                }
            }
        } while (correct);
        return playerMove;
    }
    */
}
